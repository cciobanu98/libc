/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cciobanu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 11:27:39 by cciobanu          #+#    #+#             */
/*   Updated: 2018/06/19 11:28:44 by cciobanu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/malloc.h"

t_page		*alloc_page(size_t size)
{
	t_page	*page;
	size_t	page_size;

	page_size = get_size(size);
	page = (t_page*)mmap(NULL, page_size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANON, -1, 0);
	if (page == NULL)
		return (NULL);
	page->type = get_page_type(size);
	page->size = page_size - PAGE_SIZE;
	page->next = NULL;
	page->prev = NULL;
	page->block = NULL;
	link_page(page);
	return (page);
}

void		split_block(t_block *b, size_t mem_width)
{
	t_block	*nb;

	if (b->size + BLOCK_SIZE == mem_width)
		return ;
	if (mem_width < b->size + BLOCK_SIZE * 2)
	{
		b->size = mem_width;
		return ;
	}
	nb = (t_block*)(BDATA(b) + b->size - 1);
	init_block(nb, mem_width - BLOCK_SIZE * 2 - b->size);
	nb->next = b->next;
	b->next = nb;
	nb->prev = b;
	nb->free = 1;
}

t_block		*insert_block(t_page *page, size_t size)
{
	t_block	*block;

	block = (t_block*)PDATA(page);
	if (block == NULL)
		return (NULL);
	init_block(block, size);
	if (page->type != LARGE)
		split_block(block, size);
	page->block = block;
	return (block);
}

t_block		*extend_heap(size_t size)
{
	t_block	*block;
	t_page	*page;

	page = alloc_page(size);
	if (page == NULL)
		return (NULL);
	block = insert_block(page, size);
	if (block == NULL)
		return (NULL);
	return (block);
}

void		*malloc(size_t size)
{
	t_block	*base;

	base = NULL;
	base = search_free_space_in_page(size);
	if (base == NULL)
		base = extend_heap(size);
	if (base == NULL)
		return (NULL);
	return (BDATA(base));
}
