/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_mem.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cciobanu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 11:30:14 by cciobanu          #+#    #+#             */
/*   Updated: 2018/06/19 11:30:44 by cciobanu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/malloc.h"

void		print_block(t_block *block)
{
	if (block == NULL || block->free == 1)
		return ;
	ft_printf("%p-%p: %lu octets\n", BDATA(block),
			BDATA(block) + block->size, block->size);
}

void		show_alloc_mem(void)
{
	t_page	*page;
	t_block	*block;

	page = g_head;
	if (page == NULL)
		ft_putstr("No memory alloc\n");
	else
		while (page != NULL)
		{
			ft_printf("Page: %p type:", page);
			if (page->type == TINY)
				ft_putstr("TYNE\n");
			else if (page->type == SMALL)
				ft_putstr("SMALL\n");
			else
				ft_putstr("LARGE\n");
			block = page->block;
			while (block != NULL)
			{
				print_block(block);
				block = block->next;
			}
			page = page->next;
		}
}
