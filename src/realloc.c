/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cciobanu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 11:25:26 by cciobanu          #+#    #+#             */
/*   Updated: 2018/06/19 11:25:28 by cciobanu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/malloc.h"

void	*realloc(void *ptr, size_t size)
{
	void	*to_return;
	t_block *block;

	block = search_block(ptr);
	to_return = malloc(size);
	if (block != NULL)
	{
		block->free = 1;
		ft_memcpy(to_return, BDATA(block), size);
	}
	return (to_return);
}
