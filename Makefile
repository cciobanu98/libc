# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cciobanu <ccioabnu@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/20 17:38:05 by cciobanu          #+#    #+#              #
#    Updated: 2018/01/05 00:48:24 by cciobanu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so
CFLAGS += -Wall -Wextra -Werror
DLFLAGS	= -shared -fPIC
LIB_H = libft/libft.h
PRINTF_H = ft_printf/includes/ft_printf.h
LIB = libft/libft.a
PRINTF = ft_printf/libftprintf.a

SRCFILES = free.c \
			malloc.c \
			realloc.c \
			search.c \
			show_mem.c \
			utils.c 
SRC = $(addprefix src/, $(SRCFILES))
OBJDIR = ./obj/
INCDIR = ./inc/
OBJ = $(SRCFILES:.c=.o)
OBJS = $(addprefix $(OBJDIR), $(OBJ))
OBJECTS	= $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))


all: $(NAME)

$(NAME):
	make -C libft
	make -C ft_printf
	gcc $(CFLAGS) $(LIB_H) $(PRINTF_H) -c $(SRC)
	gcc $(DLFLAGS) -o $(NAME) $(OBJ) $(LIB) $(PRINTF)
	ln -s $(NAME) libft_malloc.so

clean:
		@rm -f $(OBJ)
		rm -f libft_malloc.so
		make clean -C libft
		make clean -C ft_printf

fclean: clean 
		rm -f $(NAME)
		rm -f $(NAME)

re: fclean all
